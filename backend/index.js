const express = require('express');
const http = require('http');
const cors = require('cors');
const socketIo = require('socket.io');
const bodyParser = require('body-parser');
const mysql = require('mysql')
const { v4: uuidv4 } = require('uuid');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const app = express();
app.use(cors({
    origin: 'http://localhost:8080',
    optionsSuccessStatus: 200
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

const server = http.createServer(app);
const io = socketIo(server, {
    cors: {
        origin: "http://localhost:8080", // cors por politicas de seguridad en el front
        methods: ["GET", "POST"]
    }
});
  

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'kuepaDB'
});

connection.connect(err => {
  if (err) {
    console.error('Error connecting to MySQL:', err.stack);
    return;
  }
  console.log('Connected to MySQL as id', connection.threadId);
});

app.post('/login', (req, res) => {
    const { username, password } = req.body;
  
    connection.query('SELECT * FROM usuarios WHERE nombre_usuario = ?', [username], (error, results) => {
      if (error) {
        return res.status(500).json({ message: 'Error al consultar la base de datos' });
      }
  
      if (results.length === 0) {
        return res.status(401).json({ message: 'Credenciales inválidas' });
      }
  
      const usuario = results[0];
  
      bcrypt.compare(password, usuario.contrasena, (err, result) => {
        if (err) {
          return res.status(500).json({ message: 'Error al comparar password' });
        }
        
        if (!result) {
          return res.status(401).json({ message: 'Credenciaels invalidas' });
        }
  
        const token = jwt.sign({ username }, 'secreto', { expiresIn: '1h' });
        res.json({ token, usuario });
      });
    });
});
  

app.post('/usuarios', (req, res) => {
    console.log(req.body);
    const { username, password, role } = req.body;

    bcrypt.hash(password, 10, (error, hash) => {
        if (error) {
        console.error('Error al cifrar la contraseña:', error);
        res.status(500).json({ error: 'Error interno del servidor' });
        return;
        }

        const sql = 'INSERT INTO usuarios (nombre_usuario, contrasena, rol_id, fecha_creacion) VALUES (?, ?, ?, ?)';
        const values = [username, hash, role, new Date()];

        connection.query(sql, values, (error, results) => {
            if (error) {
                console.error('Error al insertar usuario en la base de datos:', error);
                res.status(500).json({ error: 'Error interno del servidor' });
                return;
            }
            console.log('Usuario insertado con éxito en la base de datos:', results);
            res.status(201).json({ message: 'Usuario creado exitosamente' });
        });
    });
});

app.use('/chat', (req, res, next) => {
    const token = req.headers.authorization;
    if (!token) {
        return res.status(401).json({ message: 'No autorizado' });
    }

    jwt.verify(token, 'secreto', (err, decoded) => {
        if (err) {
        return res.status(401).json({ message: 'Token inválido' });
        }
        next();
    });
});

io.on('connection', socket => {

    const userId = uuidv4(); // identificador unico de usuario
    console.log(`Usuario conectado: ${userId}`);
    console.log('New client connected');
    socket.emit('userConnected', userId);

    socket.on('disconnect', () => {
        console.log('Client disconnected');
    });

    socket.on('message', data => {
        console.log('Message received:', data);
        const sql = 'INSERT INTO mensajes (valor, nombre_usuario, fecha_creacion) VALUES (?, ?, ?)';
        connection.query(sql, [data.content, data.sender, new Date()], (err, result) => {
        if (err) throw err;

        const messageId = result.insertId;
        connection.query('SELECT me.valor as valor, me.nombre_usuario as nombre_usuario, ro.nombre_rol as rol FROM mensajes me left join usuarios us on me.nombre_usuario = us.nombre_usuario left join roles ro on us.rol_id = ro.id WHERE me.id = ? ', [messageId], (err, rows) => {
            if (err) throw err;
            io.emit('message', rows[0]);
        });
        });
    });
});

const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});